FROM debian:stretch

COPY atlassian-jira-software-7.2.4-x64.bin /tmp/install.bin
COPY installjira /tmp/installjira

RUN chmod a+x /tmp/install.bin
RUN /tmp/install.bin < /tmp/installjira

COPY ./dbconfig.xml /var/atlassian/application-data/jira/

EXPOSE 8080

COPY ./entrypoint.sh /entrypoint.sh
ENTRYPOINT /entrypoint.sh
